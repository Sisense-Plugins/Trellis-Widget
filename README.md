#	Trellis Widget Plugin

This plugin adds a new widget type, which creates a dynamic number of charts based on the data.  Of course, using SisenseWeb's standard charts you can manually create multiple widgets however you must know in advance how many to create.  If you have data security rules that limit the number of dimensions per user, you may end up in a situation where user A should see 3 charts and user B should see 5.  This plugin allows you to create one widget, that just renders a dynamic number of charts.

__Step 1: Copy the plugin__ - Copy the trellis folder into your _C:\Program Files\Sisense\PrismWeb\plugins_  directory.

__Step 2: Build your chart__ - On your dashboard, create a new widget and select Trellis as the type.  Fill in the panel items based on the descriptions below.

  * axis - For each chart, use this dimension for the axis or category.  You can leave this empty, when displaying indicators
  * value - The metric to use for drawing the charts.  If no color value is supplied, the chart colors come from this item
  * color - Optional panel, in case you want to color the charts by a different metric than the value.
  * split by - This panel is how you get multiple charts.  If your dimension has 4 unique members, the widget will build 4 charts.

__Step 3: Format your chart__ - This visualization has several formatting options, see below for a description of each.

  * Chart Size - A slider control that allows determines how big to make each chart.
  * Chart Type - Determines what kind of visualization should be created 
  * Label Location - Where to place the the Split By label
  * Aspect Ratio - Depending on the chart type, you may want to use a different aspect ratio (1:1 for pie, 16:9 for bar, etc)

__Reference/Notes__
  * When displaying indicator as the chart type, if you include an axis dimension only the first result will be displayed.
  * This sample has been confirmed working on Sisense version 6.7.1 and 7.0, and should be backwards compatible with previous version.

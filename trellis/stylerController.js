
mod.controller('stylerController', ['$scope',
    function ($scope) {

        /**
         * variables
         */


        /**
         * watches
         */
        $scope.$watch('widget', function (val) {

            $scope.model = $$get($scope, 'widget.style');
        });



        /**
         * public methods
		*/

        //  Redraw the widget
        $scope.refreshWidget = function () {         
            _.defer(function () {
                $scope.$root.widget.redraw();
            });
        };  

        //  Change the label's location
        $scope.setChartType = function (value) {         
            //  Set the value
            $scope.model.chartType = value
            //  Redraw the widget
            $scope.refreshWidget();
        }; 

        //  Change the label's location
        $scope.setValueAxis = function (value) {         
            //  Set the value
            $scope.model.valueAxis = value
            //  Redraw the widget
            $scope.refreshWidget();
        }; 

        //  Change the label's location
        $scope.setLabel = function (value) {         
            //  Set the value
            $scope.model.label = value
            //  Redraw the widget
            $scope.refreshWidget();
        }; 

        //  Change the label's location
        $scope.setAspectRatio = function (value) {         
            //  Set the value
            $scope.model.aspectRatio = value
            //  Redraw the widget
            $scope.refreshWidget();
        }; 


		
	}
]);
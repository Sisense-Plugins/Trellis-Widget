/*	Object that defines how different chart types are built 	*/
var charts = {
	'bar': function(hc, widget, data, key, chartElement, style, tooltip, clickHandler, callback, scope){

		//	Define the style for the chart title
		var titleStyle =  { "color": "#333333", "fontSize": "16px", "fontWeight": "normal" }

		//	Define highcharts object
		var options = {
			chart: {
				type: 'bar',
				events: {
					load: function(){
						callback(this.container, widget, key)
					}
				}
			},
			exporting: { enabled: false },
			legend: { enabled: false },
			title: { text: '', style: titleStyle },
			xAxis: { categories: [] },
			yAxis: {},
			series: [
				{
					name: key,
					data: []
				}
			],
			tooltip: { formatter: tooltip }
		}

		//	Handle the label placement
		if (style.label === "top"){
			options.title.text = key;
		} else if (style.label === "bottom"){
			options.title.text = key,
			options.title.verticalAlign = 'bottom';
		}  

		//	Do we need to show the title on the axis?
		var xAxisText = '';
		if ((style.label === "left") || (style.label == "right" )){
			xAxisText = key
		}

		//	Use the x axis to display the title
		options.xAxis.title = {
			text: xAxisText,
			style: titleStyle
		}

		//	Handle Y Axis
		if (style.valueAxis){
			//	Show the y axis
			options.yAxis = {
				gridLineColor: "#d1d1d7",
				gridLineDashStyle: "Dot",
				gridLineWidth: 1,
				title: { text: '' }
			}
		} else {
			//	Hide the y axis
			options.yAxis = {
				gridLineColor: "transparent",
				lineColor: "transparent",
				lineWidth: 0,
				labels: {
					enabled: false
				},
				title: { text: '' }
			}
		}

		//	Loop through each data point
		for (var i=0; i<data.y.length; i++){

			//	Build a datapoint
			var point = {
				y: data.y[i].data,
				valueLabel: data.y[i].text,
				color: data.color[i].color,
				selected: false,
				selectionData: {
					0: data.x[i].text
				},
				marker:{
					enabled: null
				}
			};

			//	Add to series
			options.series[0].data[i] = point;

			//	Add in the category for x-axis
			options.xAxis.categories[i] = data.x[i].text;
		}

		//	Instantiate the highchart
		var chartEl = jQuery(chartElement);
			chartEl.highcharts(options);
		/*
		setTimeout(function(){
			var chartEl = jQuery(chartElement);
			chartEl.highcharts(options);
				
		}, 5000)
		*/
		//var chartDivId = jQuery(chartElement).attr('id');
		//var chart = hc.chart(chartDivId, options);
	}, 
	'column': function(hc, widget, data, key, chartElement, style, tooltip, clickHandler, callback){

		//	Define the style for the chart title
		var titleStyle =  { "color": "#333333", "fontSize": "16px", "fontWeight": "normal" }

		//	Define highcharts object
		var options = {
			chart: {
				type: 'column',
				events: {
					load: function(){
						callback(this.container, widget, key)
					}
				}
			},
			exporting: { enabled: false },
			legend: { enabled: false },
			title: { text: '', style: titleStyle },
			xAxis: { categories: [] },
			yAxis: {},
			series: [
				{
					name: key,
					data: []
				}
			],
			tooltip: { formatter: tooltip }
		}

		//	Which side should the axis be on?
		var yAxisOpposite = false;

		//	Handle the label placement
		if (style.label === "top"){
			options.title.text = key;
		} else if (style.label === "bottom"){
			options.title.text = key,
			options.title.verticalAlign = 'bottom';
		} else if (style.label === "right"){
			yAxisOpposite = true;
		}  

		//	Do we need to show the title on the axis?
		var yAxisText = '';
		if ((style.label === "left") || (style.label == "right" )){
			yAxisText = key
		}

		//	Handle Y Axis
		if (style.valueAxis){
			//	Show the y axis
			options.yAxis = {
				gridLineColor: "#d1d1d7",
				gridLineDashStyle: "Dot",
				gridLineWidth: 1,
				opposite: yAxisOpposite,
				title: {
					text: yAxisText,
					style: titleStyle
				}
			}
		} else {
			//	Hide the y axis
			options.yAxis = {
				gridLineColor: "transparent",
				lineColor: "transparent",
				lineWidth: 0,
				opposite: yAxisOpposite,
				labels: {
					enabled: false
				},
				title: {
					text: yAxisText,
					style: titleStyle
				}
			}
		}

		//	Loop through each data point
		for (var i=0; i<data.y.length; i++){

			//	Build a datapoint
			var point = {
				y: data.y[i].data,
				valueLabel: data.y[i].text,
				color: data.color[i].color,
				selected: false,
				selectionData: {
					0: data.x[i].text
				},
				marker:{
					enabled: null
				}
			};

			//	Add to series
			options.series[0].data[i] = point;

			//	Add in the category for x-axis
			options.xAxis.categories[i] = data.x[i].text;
		}

		//	Instantiate the highchart
		var chart = jQuery(chartElement).highcharts(options);
		//var chartDivId = jQuery(chartElement).attr('id');
		//var chart = hc.chart(chartDivId, options);
	}, 
	'line': function(hc, widget, data, key, chartElement, style, tooltip, clickHandler, callback){

		//	Define the style for the chart title
		var titleStyle =  { "color": "#333333", "fontSize": "16px", "fontWeight": "normal" }

		//	Get color from the first data point
		var lineColor = data.y[0].color;

		//	Define highcharts object
		var options = {
			chart: {
				type: 'line',
				events: {
					load: function(){
						callback(this.container, widget, key)
					}
				}
			},
			exporting: { enabled: false },
			legend: { enabled: false },
			title: { text: '', style: titleStyle },
			xAxis: { categories: [] },
			yAxis: {},
			series: [
				{
					name: key,
					color: lineColor,
					data: []
				}
			],
			tooltip: { formatter: tooltip }
		}

		//	Which side should the axis be on?
		var yAxisOpposite = false;

		//	Handle the label placement
		if (style.label === "top"){
			options.title.text = key;
		} else if (style.label === "bottom"){
			options.title.text = key,
			options.title.verticalAlign = 'bottom';
		} else if (style.label === "right"){
			yAxisOpposite = true;
		}  

		//	Do we need to show the title on the axis?
		var yAxisText = '';
		if ((style.label === "left") || (style.label == "right" )){
			yAxisText = key
		}

		//	Handle Y Axis
		if (style.valueAxis){
			//	Show the y axis
			options.yAxis = {
				gridLineColor: "#d1d1d7",
				gridLineDashStyle: "Dot",
				gridLineWidth: 1,
				opposite: yAxisOpposite,
				title: {
					text: yAxisText,
					style: titleStyle
				}
			}
		} else {
			//	Hide the y axis
			options.yAxis = {
				gridLineColor: "transparent",
				lineColor: "transparent",
				lineWidth: 0,
				opposite: yAxisOpposite,
				labels: {
					enabled: false
				},
				title: {
					text: yAxisText,
					style: titleStyle
				}
			}
		}

		//	Loop through each data point
		for (var i=0; i<data.y.length; i++){

			//	Build a datapoint
			var point = {
				y: data.y[i].data,
				valueLabel: data.y[i].text,
				color: data.color[i].color,
				selected: false,
				selectionData: {
					0: data.x[i].text
				},
				marker:{
					enabled: null
				}
			};

			//	Add to series
			options.series[0].data[i] = point;

			//	Add in the category for x-axis
			options.xAxis.categories[i] = data.x[i].text;
		}

		//	Instantiate the highchart
		var chart = jQuery(chartElement).highcharts(options);
		//var chartDivId = jQuery(chartElement).attr('id');
		//var chart = hc.chart(chartDivId, options);
	}, 
	'pie': function(hc, widget, data, key, chartElement, style, tooltip, clickHandler, callback){

		//	Define the style for the chart title
		var titleStyle =  { "color": "#333333", "fontSize": "16px", "fontWeight": "normal" }

		//	Define highcharts object
		var options = {
			chart: {
				type: 'pie',
				events: {
					load: function(){
						callback(this.container, widget, key)
					}
				}
			},
			exporting: { enabled: false },
			legend: { enabled: false },
			title: { text: '', style: titleStyle },
			plotOptions: {},
			xAxis: { categories: [] },
			yAxis: {},
			series: [
				{
					name: key,
					data: []
				}
			],
			tooltip: { formatter: tooltip }
		}

		//	Handle the label placement
		if (style.label === "top"){
			options.title.text = key;
		} else if (style.label === "bottom"){
			options.title.text = key;
			options.title.verticalAlign = 'bottom';
		}  
		/*
		else if (style.label === "right"){
			options.title.text = key,
			options.title.verticalAlign = 'middle';	
			options.title.align = 'right';
		}  else if (style.label === "left"){
			options.title.text = key,
			options.title.verticalAlign = 'middle';	
			options.title.align = 'left';
		}  
		*/

		//	Do we hide the category labels?
		if (!style.valueAxis){
			options.plotOptions.pie = {
				dataLabels: {
					enabled: false
				}
			}
		}

		//	Loop through each data point
		for (var i=0; i<data.y.length; i++){

			//	Build a datapoint
			var point = {
				y: data.y[i].data,
				valueLabel: data.y[i].text,
				color: data.color[i].color,
				name: data.x[i].text,
				selected: false,
				selectionData: {
					0: data.x[i].text
				},
			};

			//	Add to series
			options.series[0].data[i] = point;
		}

		//	Instantiate the highchart
		var chart = jQuery(chartElement).highcharts(options);
		//var chartDivId = jQuery(chartElement).attr('id');
		//var chart = hc.chart(chartDivId, options);
	},
	'number': function(hc, widget, data, key, chartElement, style, tooltip, clickHandler, callback){

		//	Get stats about the query results
		var stats = widget.queryResult.stats,
			color = $$get(data.color[0], 'color', prism.$ngscope.dashboard.style.palette()[0]),
			value = $$get(data.y[0], 'text', ''),
			id = widget.oid + '-' + key + '-container',
			maxCharacters = stats.maxValueCharacters > stats.maxKeyCharacters ? stats.maxValueCharacters : stats.maxKeyCharacters,
			fontSize = 12;

		//	Create the label's HTML
		var text = jQuery('<div id="' + id +  '"> <div class="indicator-title"><span title="' + key + '">' + key + '</span></div>' 
					+ '<div class="indicator-value" style="color:' + color + ';"><span style="font-size:' + fontSize + 'px;">' + value + '</span></div></div>');

		//	Add event handler for when this gets added to the dom
		text.on('DOMNodeInsertedIntoDocument', function(){
			callback(this, widget, key)
		})
		

		//	Add the text label
		jQuery(chartElement).append(text);
		
	}
}

//	Define the names of each panel
var panelNames = {
	'x': 'Axis',	
	'z': 'Split By',
	'y': 'Value',
	'color': 'Color',
	'filters': 'filters'
}

//	Default key, if not split defined
var defaultKey = 'default';

// 	Register the chart type
prism.registerWidget("trelliswidget", {
	name: "trelliswidget",
	family: "chart",
	title: "Trellis Chart",
	iconSmall : "/plugins/trellis/icon-48-24.png",
	styleEditorTemplate: "/plugins/trellis/styler.html", 
	style: {
		tooltips : true, 
		independentScale: false,
		label: 'right',
		chartType: 'column',
		aspectRatio: '16:9',
		chartSize: 50,
		maxItems: 20,
		valueAxis: false
	},
	data : {
		selection : [],
		defaultQueryResult : {},	
		panels : [
			{
				name: panelNames.x,
				type: "visible",
				metadata: {
					types: ['dimensions'],
					maxitems: 1
				},
				visibility: true
			},			
			{
				name: panelNames.y,
				type: "visible",
				itemAttributes: ["color"],
				metadata: {
					types: ['measures'],
					maxitems: 1
				},
				visibility: true
			},
			{
				name: panelNames.color,
				type: "visible",
				itemAttributes: ["color"],
				metadata: {
					types: ['measures'],
					maxitems: 1
				},
				visibility: true,
				itemAdded: function(widget, item) {
                    var colorFormatType = $$get(item, "format.color.type");
                    if ("color" === colorFormatType || "color" === colorFormatType) {
                        var color = item.format.color.color;
                        defined(color) && "transparent" != color && "white" != color && "#fff" != color && "#ffffff" != color || $jaql.resetColor(item)
                    }
                    "range" === colorFormatType && $jaql.resetColor(item), defined(item, "format.color_bkp") && $jaql.resetColor(item), defined(item, "format.members") && delete item.format.members
                },
                defaultFormatter: function (widget, sourceitem) {
                    return {
                        type: "range",
                        steps: 9,
                        rangeMode: "auto"
                    };
                }
			},		
			{
				name: panelNames.z,
				type: "visible",
				canDisableItems: true,
				metadata: {
					types: ['dimensions'],
					maxitems: 1
				},
				visibility: true
			},			
			{
				name: panelNames.filters,
				type: 'filters',
				metadata: {
					types: ['dimensions'],
					maxitems: -1
				}
			}
		],

		//	Coloring function
        canColor: function(widget,panel,item) {

            return (panel.name === panelNames.color) || (panel.name === panelNames.y);
        },

        allocatePanel: function (widget, metadataItem) {

		},

        // populates the metadata items to the widget
		populateMetadata: function (widget, items) {

			var a = prism.$jaql.analyze(items);
			/*
			// allocating dimensions
			widget.metadata.panel(panelNames.nodeId).push(a.dimensions);

			// allocating measures
			widget.metadata.panel(panelNames.size).push(a.measures);

			*/

			// allocating filters
			widget.metadata.panel(panelNames.filters).push(a.filters);
		},

		//	Build the JAQL query to send to Elasticube
		buildQuery: function (widget) {
			
			//	Create the query object
			var query = { 
				datasource: widget.datasource, 
				format: "json",
				isMaskedResult:true,
				metadata: [] 
			};

			//	Loop through each panel
			for (var p in panelNames){

				//	Double check this property exists
				if (panelNames.hasOwnProperty(p)){

					//	Get the panel name
					var panelName = panelNames[p];

					//	Double check it exists
					var panel = widget.metadata.panel(panelName);
					if (panel){

						//	Loop through all items in the panel
						panel.items.forEach(function(item){

							//	Only run if not disabled
							if (!item.disabled){

								//	Is this the filters panel?
								var isFilter = (panelName === panelNames.filters);

								//	If its a filter panel, create a clone of the item
								var panelItem = null;
								if (isFilter){
									panelItem = $$.object.clone(item, true);
									panelItem.panel = 'scope',
									panelItem.filterType = 'widget';
								} else {
									panelItem = item;
								}

								//	Push into the query
								query.metadata.push(panelItem)
							}
						})
					}
				}
			}

			//	What panels are required?
			var requiredPanels = ['y'];

			//	Make sure there is at least 1 item, for each required panel
			requiredPanels.forEach(function(key){
				var panelName = panelNames[key];
				var numItems = widget.metadata.panel(panelName).items.length;
				if (numItems<1){
					query.metadata = [];
				}
			})


			return query;
		},
		
		//	Handle the results from Elasticube QUery
		processResult : function (widget, queryResult) {

			//	Build out the raw query results
			var raw = {
				metadata: queryResult.metadata(),
				values: queryResult.$$rows
			};

			//	Get the default color to use
			var defaultColor = prism.$ngscope.dashboard.style.palette()[0];

			//	Get the metadata panel items
			var axisItem = widget.metadata.panel(panelNames.x),
				valueItem = widget.metadata.panel(panelNames.y),
				splitItem = widget.metadata.panel(panelNames.z),
				colorItem = widget.metadata.panel(panelNames.color);

			//	Figure out what panels are used
			var hasAxis = axisItem.items.filter(function(i) { return !i.disabled; }).length > 0,
				hasSplit = splitItem.items.filter(function(i) { return !i.disabled; }).length > 0,
				hasColor = colorItem.items.filter(function(i) { return !i.disabled; }).length > 0;

			//	Define the column matches for each panel item
			var axisIndex = hasAxis ? 0 : -1,
				splitIndex = hasSplit ? axisIndex + 1 : -1,
				valueIndex = hasSplit ? splitIndex + 1 : axisIndex + 1,
				colorIndex = hasColor ? valueIndex + 1 : valueIndex;

			//	Initialize the data object to hold the results
			var data = {},
				table = [],
				stats = {
					'min': null,
					'max': null,
					'count': raw.values.length,
					'maxValueCharacters': 0,
					'maxKeyCharacters': 0
				};

			//	Get the measure titles
			var valueTitle = $$get(raw.metadata[valueIndex], 'jaql.title', 'Value'),
				colorTitle = (colorIndex == valueIndex) ? null : $$get(raw.metadata[colorIndex], 'jaql.title', 'Color');

			//	Loop through each row of data
			raw.values.forEach(function(row){

				//	Get the key for this split
				var key = hasSplit ? row[splitIndex].text : valueTitle;

				//	Build out the data objects
				var x = row[axisIndex] ? row[axisIndex] : {},
					y = row[valueIndex],
					color = row[colorIndex] ? row[colorIndex] : {};

				//	Make sure a color hex code is defined
				if (typeof color.color === "undefined"){
					color.color = defaultColor;
				}

				//	Create a datapoint object
				var datapoint = {
					'key': key,
					'value': y.data,
					'valueLabel': y.text,
					'valueTitle': valueTitle,
					'axis': x.text,
					'color': color.color,
					'colorValue': color.text,
					'colorTitle': colorTitle
				}

				//	Add to the regular table
				table.push(datapoint);

				//	Does the data object have a property for this key?
				if (data[key]){
					//	Yes it does, add these values to the arrays
					data[key].x.push(x);
					data[key].y.push(y);
					data[key].color.push(color);
				} else {
					//	No it doesn't, initialize the key with an array containing this point
					data[key] = {
						x: [ x ],
						y: [ y ],
						color: [ color ]
					}
				}

				//	Check on the stats
				if ((stats.min == undefined) || (stats.min > y.data) ){
					stats.min = y.data;
				}
				if ((stats.max == undefined) || (stats.max < y.data) ){
					stats.max = y.data;
				}
				if (stats.maxValueCharacters < y.text.length){
					stats.maxValueCharacters = y.text.length;
				}
				if (stats.maxKeyCharacters < key.length){
					stats.maxKeyCharacters = key.length;
				}
			})

			//	Return the calculated results
			return {
				data: data,
				table: table,
				stats: stats
			};
		}
	},

	//	Chart is being initialized
	initialized: function(widget,args) {
		
	},

	//	Adjust the JAQL query to account for filter selections
	beforequery: function(widget, args){
	},
	
	//	Data is done processing, ready to render on the page
	render : function (widget, event) {

		//	Only run, if we have data to show
		var results = widget.queryResult;
		if (results.table){

			/********************************/
	        /* 	General Chart Properties 	*/
	        /********************************/

	        //	Get a reference to the base container
	        var element = jQuery(event.element);

	        //	Define id and class names
			var divId = 'd3-trellis-' + $$get(widget, 'oid', 'new-widget'),
				divClass = 'd3-trellis';

			var newDiv = '<div id="' + divId + '" class="' + divClass + '"></div>';
			element.empty();
			element.append(newDiv);

			var container = document.getElementById(divId);

			//	Figure out the aspect ratio
			var aspectRatio = {
				x: parseInt(widget.style.aspectRatio.split(':')[0]),
				y: parseInt(widget.style.aspectRatio.split(':')[1])
			}

			//	Define the default width of each chart in pixels, based on the user's design input
			var sizing = {
					label: {
						width: 100,
						height: 20
					},
					axis: {
						height: 20
					},
					margin: {
						top: 10, 
						right: 10, 
						bottom: 10, 
						left: 10
					}
				},
				width = 400 * (widget.style.chartSize / 100),
				height = width * (aspectRatio.y / aspectRatio.x);

			//	Create an object that holds the positions/offsets to use
			var position = {
				label: widget.style.label,
				chart: {
					left: 0,
					right: 0,
					top: 0,
					bottom: 0
				}
			};
			
			/************************/
	        /* 	Event Handlers 	*/
	        /************************/

	        //	Define the zoom function
	        var showTooltip = function(node){
	        	
	            //	Get the dom injector
				var myDom = prism.$injector.get('ux-controls.services.$dom');			

				//	Define the modal window's options
				var options = {
					scope: { 
						marker: node
					},
					templateUrl: '/plugins/trellis/tooltipTemplate.html'				
				};

				//	Define the ui options	
				var ui = {
	                css: 'area-map-tip',
	                placement: {
	                    place: 'r',
	                    anchor: 't'
	                },
	                initialShowDelay: 0,
	                betweenShowDelay: 0
	            };

				//	Create the tooltip, and save to the 
				node.tooltip = myDom.tip(options,ui);

				//	Define where to put the tooltip
				var offset = $(this).offset();
				var tipLocation = { 
					//x: node.px, 
					//y: node.py, 
					x: offset.left + 40,
					y: offset.top - 60,
					space: 0
				}

				//	Activate the tooltip
				node.tooltip.activate(tipLocation);			
	        }

	        //	Mouseout handler function
	        var hideTooltip = function(node){

	        	//	Decactivate the tooltip
	            node.tooltip.deactivate();
	        }

	        //	Highcharts tooltip function
	        var tooltip = function(){
	        	//	Build some HTML for the tooltip to display
	        	var html = '<b>'  + this.series.name + '(' + this.key + ')</b>: ' + this.point.valueLabel;
	        	//	Return it
	        	return html;
	        }

	        //	On click
	        var clickHandler = function(node){

	        }

	        //	Callback function for when a chart has fully loaded
	        var chartFinished = function(element, widget, key){

	        	//	Update the counter
	        	widget.options.trellis.renderStatus[key] = true;

	        	//	Is the widget finished loading all charts?
	        	var isFinished = true;
	        	for (var k in widget.options.trellis.renderStatus){
	        		if (widget.options.trellis.renderStatus.hasOwnProperty(k)){
	        			if (!widget.options.trellis.renderStatus[k]){
	        				isFinished = false;
	        			}
	        		}
	        	}

	        	//	Is the widget finished loading all charts?
				if (isFinished) {

					//	Special handler for indicators
					if (widget.style.chartType == "number"){

						//	Figure out the width of each 
						var padding = 10,
							width = jQuery(element).parent().width() - padding,
							height = jQuery(element).parent().height() - padding;

						//	Get all the labels
						var labels = jQuery('div.indicator-value > span', container);

						//	Figure out the widest label
						var widestLabel = null;
						var size = 0;
						labels.each(function(i){
							var lengthy = jQuery(this).text().length;
							if (lengthy > size){
								size = lengthy;
								widestLabel = jQuery(this);
							}
						})

						//	Recursive function that makes the font size bigger, until it maxes out the container
						function resizer(){
							//	Get the current font size
							var fs = parseFloat(widestLabel.css('font-size'));
							//	Increase by 1px
							labels.css('font-size',fs+1);
							//	Is it now too big for the container?
							if ((widestLabel.height() >= height) || (widestLabel.width() >= width)){
								//	Yes, reduce by 1
								labels.css('font-size', fs);
								//	And trigger the domready
								widget.trigger("domready");
							} else {
								//	Nope, keep trying
								resizer();
							}
						}
						
						//	Kick off the resizer function
						resizer();
						
					} else {

						//	Trigger the domready event
						widget.trigger( "domready");
					}
				}
	        }

	        /************************/
	        /* 	Chart Creation	 	*/
	        /************************/

	        //	Get the chart creation function
			var chartCreator = charts[widget.style.chartType];
			if (typeof chartCreator == "function"){

				//	Init the chart renderer tracker
				widget.options.trellis = {
					renderStatus: {},
					numberCharts: Object.keys(widget.queryResult.data).length,
					numberRendered: 0
				};
				
				//	Add a rendering check for each split key
				for (var key in results.data){
					if (results.data.hasOwnProperty(key)){
						widget.options.trellis.renderStatus[key] = false;
					}
				}

				//	Get a reference to the highcharts object
				var hc = prism.$injector.get("basicchart.services.$highchart");

		        //	Create a div element for each chart
				for (var key in results.data){
					if (results.data.hasOwnProperty(key)){

						//	Get the data for this chart
						var data  = results.data[key];

						//	Define the div id
						var divId = widget.oid + '-' + key;

						//	Create the div element
						var newDiv = jQuery('<div id="' + divId + '" class="trellis" style="height:'+height+'px; width:'+width+'px;"></div>');
						newDiv.on('DOMNodeInsertedIntoDocument', function(event){
							//	Element has been added to the DOM, create the chart now 
							chartCreator(hc, widget, data, key, this, widget.style, tooltip, clickHandler, chartFinished, event.$scope);
						})
						
						//	Add the new div container		
						jQuery(container).append(newDiv);
					}
				}
			}
		}
	},

    options: {
        dashboardFiltersMode: "slice",
        selector: false,
        title: false
    },
    
    sizing: {
        minHeight: 30, //header
        maxHeight: 2048,
        minWidth: 300,
        maxWidth: 2048,
        height: 500,
        defaultWidth: 512
    }
});